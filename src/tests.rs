//    merkle-rs, a merkle tree library implemented in Rust
//    Copyright (C) 2016 Waylon Cude
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Lesser General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Lesser General Public License for more details.
//
//    You should have received a copy of the GNU Lesser General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#[cfg(test)]
mod tests {
    use std::iter::FromIterator;
    use MerkleTree;
    use sha3::Sha3;
    use sha2::Sha512;
    use sha2::Sha256;
    #[test]
    fn two_word_owned_string_test() {
        assert_eq!(
            "80938d92fe5d216884786ab8c23b87dc1c572e603c5b5d84d2454fa9bebbc84afa1d7a33ed35ed0196a023043e29c8f67b452102571e0f1944b8381b213a81fb",
            &MerkleTree::from_iter(vec!["test".to_owned(),"string".to_owned()]).get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn sequential_pop_test() {
        let words = vec!["one","two","three","four","five"];
        let mut m_tree = MerkleTree::new(words);
        assert_eq!(
            "e67541867b37e7f30f55d88d36498513555a6f79e5363db126f0222a2e9de9778a69bb2ea4012b64e7f0864919a129a5ddc9c7ec2b669307aab146302c974a73",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
        println!("{:?}",m_tree);
        m_tree.pop();
        println!("{:?}",m_tree);
        assert_eq!(
            "9768cf06a3d08ca9f8a1c532adc538657b996a44c4dea1f848900ee2194dabe2df0362bb031a2c761ed553f2452da142fadd9bd0d71bf5501672c63aeead3be6",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
        m_tree.pop();
        println!("{:?}",m_tree);
        assert_eq!(
            "428e0165d9540d82a928ca1da1e9b3358988c352ead4cfe34f9955656b316004d06c055962457be7ee6b4c5291a4361fd64594522d9bc213957fd5f7cb0d1c50",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
        m_tree.pop();
        println!("{:?}",m_tree);
        assert_eq!(
            "7798aa470d68101a0b2642f504c3c28b3d621f48ea64a7e8a8b45fdb6214fab83cb03e638f8c4f56ff4e7549251cc630e89ef90e5a6153289ef7ef2d8bccae92",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
        m_tree.pop();
        println!("{:?}",m_tree);
        assert_eq!(
            "94e0c8a1a40de2a30c5b3e0f6d7f5ee4a4cd8e563c47261666100e27b318e360e1667ad44237cee6ae772882cc65ef7e1c0ee5d0c4164afd44fda326f01eee5a",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
        m_tree.pop();
        println!("{:?}",m_tree);
        assert_eq!(
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
            &m_tree.get_root_hash(Sha3::sha3_512())
        );
    }
    #[test]
    fn zero_word_test() {
        let words: Vec<String> = Vec::new();
        assert_eq!(
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
            &MerkleTree::from_iter(words.clone()).get_root_hash(Sha3::sha3_512())
            );
        assert_eq!(MerkleTree::from_iter(words.clone()).words,words);
    }
    #[test]
    fn four_word_test() {
        let words = vec!["one","two","three","four"];
        assert_eq!(
            "9768cf06a3d08ca9f8a1c532adc538657b996a44c4dea1f848900ee2194dabe2df0362bb031a2c761ed553f2452da142fadd9bd0d71bf5501672c63aeead3be6",
            &MerkleTree::from_iter(words).get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn zero_word_pop_test() {
        let words = vec!["one","two","three","four"];
        let mut m_tree = MerkleTree::from_iter(words.clone());
        assert_eq!("four",&m_tree.pop().unwrap());
        assert_eq!("three",&m_tree.pop().unwrap());
        assert_eq!("two",&m_tree.pop().unwrap());
        assert_eq!("one",&m_tree.pop().unwrap());
        assert_eq!(None,m_tree.pop());
        assert_eq!(
            "a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26",
            &m_tree.get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn three_word_pop_test() {
        let words = vec!["one","two","three","four"];
        let mut m_tree = MerkleTree::from_iter(words.clone());
        assert_eq!("four",&m_tree.pop().unwrap());
        assert_eq!(
            "428e0165d9540d82a928ca1da1e9b3358988c352ead4cfe34f9955656b316004d06c055962457be7ee6b4c5291a4361fd64594522d9bc213957fd5f7cb0d1c50",
            &m_tree.get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn three_word_test() {
        let words = vec!["one","two","three"];
        assert_eq!(
            "428e0165d9540d82a928ca1da1e9b3358988c352ead4cfe34f9955656b316004d06c055962457be7ee6b4c5291a4361fd64594522d9bc213957fd5f7cb0d1c50",
            &MerkleTree::from_iter(words.clone()).get_root_hash(Sha3::sha3_512())
            );
        assert_eq!(MerkleTree::from_iter(words.clone()).words,words);
    }
    #[test]
    fn one_word_push_test() {
        let words: Vec<String> = vec![];
        let mut m_tree = MerkleTree::from_iter(words);
        m_tree.push("test");
        assert_eq!(
            "64c37f15ca73f04d4ab34f6f9388dc63860e630efc3011def48c4e625c9c37616df5017df9461104b28aad2b874a5b464757398b83c24ba08eb57d57d9d83dfe",
            &m_tree.get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn three_word_push_test() {
        let words = vec!["one","two"];
        let mut m_tree = MerkleTree::from_iter(words);
        m_tree.push("three");
        assert_eq!(
            "428e0165d9540d82a928ca1da1e9b3358988c352ead4cfe34f9955656b316004d06c055962457be7ee6b4c5291a4361fd64594522d9bc213957fd5f7cb0d1c50",
            &m_tree.get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn five_word_test() {
        let words = vec!["one","two","three","four","five"];
        assert_eq!(
            "e67541867b37e7f30f55d88d36498513555a6f79e5363db126f0222a2e9de9778a69bb2ea4012b64e7f0864919a129a5ddc9c7ec2b669307aab146302c974a73",
            &MerkleTree::from_iter(words.clone()).get_root_hash(Sha3::sha3_512())
            );
        assert_eq!(MerkleTree::from_iter(words.clone()).words,words);
    }
    #[test]
    fn five_word_push_test() {
        let words = vec!["one","two","three"];
        let mut m_tree = MerkleTree::new(words);
        m_tree.push("four");
        m_tree.push("five");
        assert_eq!(
            "e67541867b37e7f30f55d88d36498513555a6f79e5363db126f0222a2e9de9778a69bb2ea4012b64e7f0864919a129a5ddc9c7ec2b669307aab146302c974a73",
            &m_tree.get_root_hash(Sha3::sha3_512())
            );
    }
    #[test]
    fn one_word_test() {
        let words = vec!["test"];
        assert_eq!(
            "64c37f15ca73f04d4ab34f6f9388dc63860e630efc3011def48c4e625c9c37616df5017df9461104b28aad2b874a5b464757398b83c24ba08eb57d57d9d83dfe",
            &MerkleTree::from_iter(words.clone()).get_root_hash(Sha3::sha3_512())
            );
        assert_eq!(MerkleTree::from_iter(words.clone()).words,words);
    }
    #[test]
    fn validate_test() {
        let words = vec!["validate","test"];
        assert!(MerkleTree::from_iter(words).validate(
                Sha3::sha3_512(),
                "6a3c84cd42729f47a39c37db30a06a54505b4213d84393811cf5861e1c2401c15c747841c392d98b9c04863fc4e6cf68a664c9853c2aadf2845b968b68254255")
            );
    }
    #[test]
    fn validate_new_convenience_test() {
        let words = vec!["validate","test"];
        assert!(MerkleTree::new(words).validate(
                Sha3::sha3_512(),
                "6a3c84cd42729f47a39c37db30a06a54505b4213d84393811cf5861e1c2401c15c747841c392d98b9c04863fc4e6cf68a664c9853c2aadf2845b968b68254255")
            );
    }
    #[test]
    fn validate_sha256_test() {
        let words = vec!["validate","test"];
        assert!(MerkleTree::from_iter(words).validate(
                Sha256::new(),
                "f4a7325e2c953c1afcb04bee766758a3c822b7bc999e6f1047f5796f8b1a0dc6")
            );
    }
    #[test]
    fn validate_sha512_test() {
        let words = vec!["validate","test"];
        assert!(MerkleTree::from_iter(words).validate(
                Sha512::new(),
                "d1958da1732a08fbd51206e388bb514fe1da13e50a4cc9fa997807f8f78a0c76d639d1e149c69505a9c1d15934dde3e91e29f56be4787482cd26f334acd12fd7")
            );
    }
}
