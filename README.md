Merkle-rs
=========
A Merkle tree library written in Rust.

[Documentation](https://waylon531.gitlab.io/merkle-rs/merkle/index.html)

Usage
=====
Add this crate as a dependency in your Cargo.toml
```
[dependencies]
merkle = { git = "https://gitlab.com/waylon531/merkle-rs" }
```

Example
=======

```rust
extern crate merkle;
use merkle::MerkleTree;
use merkle::sha3::Sha3;
let words = vec!["example","words"];
let mut m_tree = MerkleTree::new(words);
assert_eq!(
    m_tree.get_root_hash(Sha3::sha3_512()),
    "00766ae74c3ba18189e8ed33b218b1f491b99d\
    cccd13ce046d97e30bdcb2d4bbe0dcaeb29dbe5\
    88fa5f7b71f36729be21f5077c89c62380f1a32\
    2db23c1c3edc"
);
```

Benchmark results:

```
test tests::tests::bench_1024_nodes_creation      ... bench:     236,353 ns/iter (+/- 13,864)
test tests::tests::bench_1024_nodes_hash_sha3_512 ... bench:   4,624,096 ns/iter (+/- 278,386)
test tests::tests::bench_1024_nodes_hash_sha512   ... bench:   1,423,030 ns/iter (+/- 112,639)
test tests::tests::bench_1025_nodes_creation      ... bench:     251,627 ns/iter (+/- 25,867)
test tests::tests::bench_1025_nodes_hash_sha3_512 ... bench:   8,804,895 ns/iter (+/- 285,680)
test tests::tests::bench_1025_nodes_hash_sha512   ... bench:   2,797,397 ns/iter (+/- 95,767)
test tests::tests::bench_2048_nodes_hash_sha3_512 ... bench:   8,834,907 ns/iter (+/- 227,028)
test tests::tests::bench_2049_nodes_hash_sha3_512 ... bench:  17,733,985 ns/iter (+/- 691,703)
test tests::tests::bench_single_node_creation     ... bench:         162 ns/iter (+/- 5)
```

Most of the time is spent in crypto in the digest function.
